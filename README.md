# Task

This assignment will test you knowledge in React, SlateJS and Slate-plugins.
The goal is to:

- Use the Rich Text example from SlateJS to start building your core editor
- Add a toolbar button that if pressed splits the line that you’re on into two paragraphs based on where the carat is positioned. If the carat is at the beginning or and of a line then nothing should happen.

Use the slate-plugins `EditablePlugins` Atomic implementation design as showcased by their examples to develop a plugin that:

- Create a bordered block that is editable (text can be injected)
- The border should have a 2px black border if less than 100 characters is in the block
- The border should change to 4px red border if more than 100 characters

You will be judged on the following criteria:

- Code cleanliness and design
- Documentation provided
- Code modularity
- Adhering to best practices

# If issues are still actual

```dotnetcli
node_modules/react-scripts/scripts/utils/verifyTypeScriptSetup.js:239
      appTsConfig.compilerOptions[option] = value;
TypeError: Cannot assign to read only property 'jsx' of object '#<Object>'
```

1. Go to node_modules/react-scripts/scripts/utils/verifyTypeScriptSetup.js
2. Change line 238 to: } else if (parsedCompilerOptions[option] !== valueToCheck && option !== "jsx") {

```
appTsConfig.compilerOptions[option] = suggested;
TypeError: Cannot add property allowSyntheticDefaultImports, object is not extensible
```

Before:

let result;
parsedTsConfig = immer(readTsConfig, config => {
result = ts.parseJsonConfigFileContent(
config,
ts.sys,
path.dirname(paths.appTsConfig)
);
});
After:

parsedTsConfig = {...readTsConfig};

const result = ts.parseJsonConfigFileContent(
parsedTsConfig,
ts.sys,
path.dirname(paths.appTsConfig)
);
