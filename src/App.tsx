import React from 'react';
import './App.css';
import SlateEditorComponent from './components/SlateEditorComponent';

function App() {
  return (
    <div className="App">
      <SlateEditorComponent />
    </div>
  );
}

export default App;
