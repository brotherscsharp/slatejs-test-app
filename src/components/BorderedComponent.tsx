import { Editor } from 'slate';
import React from 'react';
import { useSlate } from 'slate-react';

export const BorderedComponent = (props) => {
  const { attributes, children, element } = props;
  const editor = useSlate();
  let offset = 0;
  for (const point of Editor.positions(editor)) {
    offset += point.offset;
  }

  return (
    <p {...attributes} contentEditable>
      <div
        aria-multiline
        style={offset > 100 ? { border: '4px solid rgb(252, 0, 0)' } : { border: '2px solid rgb(0, 0, 0)' }}
      >
        {children}
      </div>
    </p>
  );
};
