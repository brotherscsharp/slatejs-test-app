import React from 'react';
import Icon from 'react-icons-kit';
import { useSlate } from 'slate-react';
import { isBlockActive, toggleBlock } from '../../helpers/toolbarHelpers';
import { Button } from './Button';

export const BlockButton = ({ format, icon }: any) => {
  const editor = useSlate();
  return (
    <Button
      active={isBlockActive(editor, format)}
      onMouseDown={(event: any) => {
        event.preventDefault();
        toggleBlock(editor, format);
      }}
    >
      <Icon icon={icon} />
    </Button>
  );
};
