import React from 'react';
import Icon from 'react-icons-kit';
import { useSlate } from 'slate-react';
import { toggleAddNewBlock } from '../../helpers/toolbarHelpers';
import { Button } from './Button';

export const EditableBlockButton = ({ icon }: any) => {
  const editor = useSlate();
  return (
    <Button
      onMouseDown={(event: any) => {
        event.preventDefault();
        toggleAddNewBlock(editor);
      }}
    >
      <Icon icon={icon} />
    </Button>
  );
};
