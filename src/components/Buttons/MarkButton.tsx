import React from 'react';
import Icon from 'react-icons-kit';
import { useSlate } from 'slate-react';
import { isMarkActive, toggleMark } from '../../helpers/toolbarHelpers';
import { Button } from './Button';

export const MarkButton = ({ format, icon }: any) => {
  const editor = useSlate();
  return (
    <Button
      active={isMarkActive(editor, format)}
      onMouseDown={(event: any) => {
        event.preventDefault();
        toggleMark(editor, format);
      }}
    >
      <Icon icon={icon} />
    </Button>
  );
};
