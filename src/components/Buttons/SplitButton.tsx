import React from 'react';
import Icon from 'react-icons-kit';
import { useSlate } from 'slate-react';
import { isSplitActive, toggleSplit } from '../../helpers/toolbarHelpers';
import { Button } from './Button';

export const SplitButton = ({ icon }: any) => {
  const editor = useSlate();
  return (
    <Button
      active={isSplitActive(editor)}
      onMouseDown={(event: any) => {
        event.preventDefault();
        toggleSplit(editor);
      }}
    >
      <Icon icon={icon} />
    </Button>
  );
};
