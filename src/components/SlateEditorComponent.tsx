import React, { useCallback, useMemo, useState } from 'react';
import isHotkey from 'is-hotkey';
import { Editable, withReact, Slate, RenderElementProps, RenderLeafProps } from 'slate-react';
import { createEditor, Node } from 'slate';
import { withHistory } from 'slate-history';

import { ic_format_bold } from 'react-icons-kit/md/ic_format_bold';
import { ic_format_italic } from 'react-icons-kit/md/ic_format_italic';
import { ic_code } from 'react-icons-kit/md/ic_code';
import { ic_format_underlined } from 'react-icons-kit/md/ic_format_underlined';
import { ic_looks_one } from 'react-icons-kit/md/ic_looks_one';
import { ic_looks_two } from 'react-icons-kit/md/ic_looks_two';
import { ic_format_quote } from 'react-icons-kit/md/ic_format_quote';
import { ic_format_list_numbered } from 'react-icons-kit/md/ic_format_list_numbered';
import { ic_list } from 'react-icons-kit/md/ic_list';
import { ic_keyboard_return } from 'react-icons-kit/md/ic_keyboard_return';
import { ic_crop_landscape } from 'react-icons-kit/md/ic_crop_landscape';
import { BorderedComponent } from './BorderedComponent';
import { toggleMark } from '../helpers/toolbarHelpers';
import { MarkButton } from './Buttons/MarkButton';
import { BlockButton } from './Buttons/BlockButton';
import { Toolbar } from './Toolbar';
import { EditableBlockButton } from './Buttons/EditableBlockButton';
import { SplitButton } from './Buttons/SplitButton';

const HOTKEYS = {
  'mod+b': 'bold',
  'mod+i': 'italic',
  'mod+u': 'underline',
  'mod+`': 'code',
};

const SlateEditorComponent = () => {
  const [value, setValue] = useState<Node[]>(initialValue);
  const renderElement = useCallback((props) => <Element {...props} />, []);
  const renderLeaf = useCallback((props) => <Leaf {...props} />, []);
  const editor = useMemo(() => withHistory(withReact(createEditor())), []);

  return (
    <Slate editor={editor} value={value} onChange={(value) => setValue(value)}>
      <Toolbar>
        <MarkButton format="bold" icon={ic_format_bold} />
        <MarkButton format="italic" icon={ic_format_italic} />
        <MarkButton format="underline" icon={ic_format_underlined} />
        <MarkButton format="code" icon={ic_code} />
        <BlockButton format="heading-one" icon={ic_looks_one} />
        <BlockButton format="heading-two" icon={ic_looks_two} />
        <BlockButton format="block-quote" icon={ic_format_quote} />
        <BlockButton format="numbered-list" icon={ic_format_list_numbered} />
        <BlockButton format="bulleted-list" icon={ic_list} />
        <EditableBlockButton format="bordered-block" icon={ic_crop_landscape} />
        <SplitButton icon={ic_keyboard_return} />
      </Toolbar>
      <Editable
        renderElement={renderElement}
        renderLeaf={renderLeaf}
        placeholder="Enter some rich text…"
        spellCheck
        autoFocus
        onKeyDown={(event) => {
          for (const hotkey in HOTKEYS) {
            if (isHotkey(hotkey, event as any)) {
              event.preventDefault();
              // @ts-ignore
              const mark = HOTKEYS[hotkey];
              toggleMark(editor, mark);
            }
          }
        }}
      />
    </Slate>
  );
};

const Element = (props: RenderElementProps) => {
  const { attributes, children, element } = props;
  switch (element.type) {
    case 'block-quote':
      return <blockquote {...attributes}>{children}</blockquote>;
    case 'bulleted-list':
      return <ul {...attributes}>{children}</ul>;
    case 'heading-one':
      return <h1 {...attributes}>{children}</h1>;
    case 'heading-two':
      return <h2 {...attributes}>{children}</h2>;
    case 'list-item':
      return <li {...attributes}>{children}</li>;
    case 'numbered-list':
      return <ol {...attributes}>{children}</ol>;
    case 'bordered-block':
      return <BorderedComponent {...props} />;
    default:
      return <p {...attributes}>{children}</p>;
  }
};

const Leaf = ({ attributes, children, leaf }: RenderLeafProps) => {
  if (leaf.bold) {
    children = <strong>{children}</strong>;
  }

  if (leaf.code) {
    children = <code>{children}</code>;
  }

  if (leaf.italic) {
    children = <em>{children}</em>;
  }

  if (leaf.underline) {
    children = <u>{children}</u>;
  }

  return <span {...attributes}>{children}</span>;
};

const initialValue = [
  {
    type: 'paragraph',
    children: [
      { text: 'This is editable ' },
      { text: 'rich', bold: true },
      { text: ' text, ' },
      { text: 'much', italic: true },
      { text: ' better than a ' },
      { text: '<textarea>', code: true },
      { text: '!' },
    ],
  },
  {
    type: 'paragraph',
    children: [
      {
        text: "Since it's rich text, you can do things like turn a selection of text ",
      },
      { text: 'bold', bold: true },
      {
        text: ', or add a semantically rendered block quote in the middle of the page, like this:',
      },
    ],
  },
  {
    type: 'block-quote',
    children: [{ text: 'A wise quote.' }],
  },
  {
    type: 'paragraph',
    children: [{ text: 'Try it out for yourself!' }],
  },
];

export default SlateEditorComponent;
